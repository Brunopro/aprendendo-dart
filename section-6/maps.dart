class InfosPessoa {
  int idade;
  InfosPessoa(this.idade);
}

void main(){
  //Maps trabalham com key e value
  //onde a chave pode ser nome e o valor "Bruno"
  //ou cidade e o valor São Paulo
  Map<int, String> ddds = Map();
  ddds[11] = "São Paulo";
  ddds[86] = "Piaui";
  ddds[31] = "Minas Gerais";

  print(ddds.keys);
  print(ddds.values);
  print(ddds);

  ddds.remove(11);

  print(ddds);

  Map<String, dynamic> pessoa = Map();
  pessoa["nome"] = "Enzo";
  pessoa["idade"] = 10;
  pessoa["altura"] = 1.50;

  Map<String, InfosPessoa> pessoas = Map();
  pessoas["João"] = InfosPessoa(30);
  pessoas["João"] = InfosPessoa(20);
}