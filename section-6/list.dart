class Pessoa {
  String nome;
  int idade;

  Pessoa(this.nome, this.idade);
}

void main() {
  List<String> nomes = ["Daniel", "Marcos", "Maria", "Thiago"];

  print(nomes);

  //Listando nomes usando forEach e asMap
  nomes.asMap().forEach((i, item) {
    print('$i $item');
  });

  print(nomes[0]);

  nomes.add("Agnes");

  print(nomes);

  print(nomes.length);

  //remover item da posicão 2
  nomes.removeAt(2);

  print(nomes);

  //adicionar item a lista definindo posição
  nomes.insert(0, "Bruno");

  print(nomes);

  print("Listas contém Nazaré? : ${nomes.contains("Nazaré")}");
  print("Listas contém Bruno? : ${nomes.contains("Bruno")}");

  //Criado lista de Objetos
  List<Pessoa> pessoas = List();

  Pessoa pessoa1 = Pessoa("Bruno", 24);
  Pessoa pessoa2 = Pessoa("Maria", 20);

  pessoas.add(pessoa1);
  pessoas.add(pessoa2);

  print(pessoas);
  print(pessoas[0]);

  for (Pessoa p in pessoas) {
    print(p.nome);
    print(p.idade);
  }

  for (Pessoa p in pessoas) {
    if(p.nome == "Bruno"){
      print("Tem um Bruno na lista");
    }
  }


  //me retornar o objeto que satifaz meu teste
  var object = pessoas.where((p) => p.nome == "Bruno");

  //Vai armazenar aqui um objeto que contém o nome Bruno
  print(object.map((o) => "nome: ${o.nome}, idade: ${o.idade}").toList());

  //every retorna true só se todos os elementos satisfazerem o teste
  if (pessoas.every((p) => p.idade >= 18)) {
    print("Todos os elementos da lista tem idade maior igual a 18");
  } else {
    print("Alguns elementos ou todos contém idade menor que 18");
  }
}
