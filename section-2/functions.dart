void main(){
  printIntro();
  printIntro();


  print('----------------------------');

  calcSoma(10, 5.5);

  print('----------------------------');

  double returnCalc = calcMult(10, 2.5);

  print('retorno da função de multiplicação: $returnCalc');


  print('------------------------------------------------------');

  double returnArea = calcAreaCirculo(50);

  print(calcAreaCirculo(50));

  print('area do circulo é igual $returnArea');

  print('--------------------------------------------------------');

  criarBotao('Sair', botaoCriado);

  criarBotao('Entrar', botaoCriado, cor: "Vermelho", largura: 150);

  criarBotao('Sair', (){
    print('botão criado por function anônima');
  });
}

void printIntro(){
  print("Seja bem-vindo(a)!");
  print("Escolha a opção");
}

void calcSoma(num a, num b){
  num result = a + b;
  print(result);
}

double calcMult(double a, double b){
  double res = a * b;
  return res;
}

//outra forma de criar função
double calcAreaCirculo(double raio) => 3.14 * raio * raio;

void botaoCriado(){
  print("Botão criado!!!");
}

//função com parâmetros opcionais
void criarBotao(String texto, Function criadoFunc, {String cor, double largura}){
  
  cor = cor == null ? 'Azul': cor;
  //outra forma de fazer isso
  //print(cor?? "Preto");

  largura = largura == null ? 100 : largura;
  
  
  //imprimindo variaveis
  //print('${[texto, cor, largura]}');
  //formas de printar varias variables em um unico print
  print('The values are: $texto, $cor, $largura');
  //['The values are:', x, y, z].forEach(print);


  criadoFunc();
  print('----------------------------------------');
}

