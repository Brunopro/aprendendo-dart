void main() {
  double nota = 10.0;

  if(nota < 5.0){
    print("Não passou no exame ):");
  } else if (nota == 0.0){
    print("Zerou a prova");
  }else {
    print("Passou no exame!");
    if(nota == 10){
      print("Tirou nota maxima!");
    } else {
      print(nota);
    }
  }

  print("-------------------------------------------------------");


  //Operador ternario
  bool aprovado = true;
  String info;
  String nome = "Bruno";

  info = aprovado? "Aprovado!!!" : "Reprovado...";


  print(info);

  info = nota > 5.0 ? "Nota maior que 5.0": "Nota menor que 5.0";

  print(info);

  info = nome == "Bruno"? "Aluno": "Não é aluno";

  print(info);
}