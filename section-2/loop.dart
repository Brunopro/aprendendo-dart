void main() {
  //for
  //primeiro parametro: inicialização do valor
  //segundo parametro: condição
  //terceiro parametro: incremento
  print("de 0 a 10");

  for (int i = 0; i <= 10; i++) {
    print(i);
  }

  print("de 3 em 3");

  for (int i = 0; i <= 10; i+=3) {
    print(i);
  }

  String name = "Bruno";
  int i = 0;

  while(name == "Bruno"){
    print("Mesmo nome");
    i++;
    if(i == 5){
      name = "Daniel";
      print("Nome mudou");
    }
  }

  print('--------------- Do While ----------------');
  //do-while
  //do-while funciona assim, do = faça primeiro, while = verifica depois, mesmos que na primeira vez a condição não seja atendida
  int k = 0;
  do {
    print(k);
    k++;
  } while (k < 1);

  //do executa mesmo que k ja seja menor do que 1;
}
