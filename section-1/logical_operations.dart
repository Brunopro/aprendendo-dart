void main(){

  // Comparadores 
  // ----------------------
  // > maior que 
  // >= maior ou igual que
  // < menor que
  // <= menor ou igual que
  // == igual
  // != diferente

  bool testeComparacao = (10 > 20);
  print("testeComparacao 10 é maior que 20?: $testeComparacao");

  print("10 é igual a 20?: ${ 10 == 20 }");
  print("10 é menor que 20?: ${ 10 < 20 }");
  print("10 é diferente de 20?: ${ 10 != 20 }");

print('-------------------------------------------------------');
  print('------------------- operador or ----------------------');
  print('-------------------------------------------------------');

  //Operador OR (||) pipe
  //-------------------
  // O operardor or só retornar true quando o primeiro ou o segundo ou os dois forem true
  // true true -> true (se tiver dois valores true, ele vai retornar true)
  // true false -> true (se tiver um valor false e um verdadeiro e vai retornar true)
  // false true -> true
  // false false -> false (se tiver dois valores falsos ele vai retornar false)

  bool testeOr = (true || false);
  print('testeOr vai receber true ou false?: $testeOr');

  testeOr = (false || false);
  print('testeOr false or false?: $testeOr');

  print('-------------------------------------------------------');
  print('------------------- operador and ----------------------');
  print('-------------------------------------------------------');

  // Operador AND
  // ------------------------------------------
  // Operador AND só retornar true quando a primeira operação e a segunda operação for true
  // true && true return true
  // true && false return false
  // false && true return false
  // false && false return false

  bool testeAnd = (false && true);
  print("testeAnd false and true: $testeAnd");
  
  testeAnd = (true && true);
  print("testeAnd true and true: $testeAnd");

  //--------------------------------------------------------------
  //expressões complexas

  print('----------------------------------------');

  print('10 maior que 20?  = false, (30 menor que 20? = false, false or false = false) , false and false = false  : result =  ${ (10 > 20) && ((30 < 20 ) || false)}');


  //--------------------------------------------------------------
  //Operador NOT inverte o valor do booleano se for false muda pra true se for true muda pra false
  print('-----------------operador NOT -------------------------------------------');
  print('!false : ${ !false }');
  print('!true : ${ !true }');
}