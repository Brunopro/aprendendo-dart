class Pessoa {
  String nome;
  int idade;
  double altura;

  //quando não declaramos o construtor, por padrão ele é declarado
  //desta forma Pessoa(); mesmo que não apareça

  /* forma com código maior
  Pessoa(String nome, int idade, double altura){
    this.nome = nome;
    this.idade = idade;
    this.altura = altura;
  }
  */


  //forma com código menor
  Pessoa(this.nome, this.idade, this.altura);

  //posso criar um construtor personalizado que não receba todos os dados
  //vai ser o construtor .nascer()
  Pessoa.nascer(this.nome, this.altura){
    this.idade = 0;

    print("$nome Nasceu!!");
    print("Altura do bebe $altura cm");
  }

}

void main(){

  Pessoa pessoa1 = Pessoa("Bruno", 24, 1.80);
  print("altura do ${pessoa1.nome} é ${pessoa1.altura} metros");


  //Testando construtor nascer
  Pessoa.nascer("Enzo", 0.30);

  //Instanciando um objeto com o construtor nascer
  Pessoa nene = Pessoa.nascer("Natalia", 0.20);

  print(nene.nome);

}