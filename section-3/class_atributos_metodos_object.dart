class Pessoa {
  String nome;
  int idade;
  double altura;

  void dormir(){
    print("$nome está dormindo!");
  }

  void aniver(){
    idade++;
    print("$nome agora tem $idade anos");
  }
}

void main(){

  Pessoa pessoa1 = Pessoa();

  pessoa1.nome = "Bruno";
  pessoa1.idade = 24;
  pessoa1.altura = 1.80;
  pessoa1.aniver();
  pessoa1.dormir();

  print("altura do ${pessoa1.nome} é ${pessoa1.altura} metros");


}