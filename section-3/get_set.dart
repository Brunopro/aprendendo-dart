class Pessoa {
  String nome;
  int _idade;
  double altura;

  //quando não declaramos o construtor, por padrão ele é declarado
  //desta forma Pessoa(); mesmo que não apareça

  /* forma com código maior
  Pessoa(String nome, int idade, double altura){
    this.nome = nome;
    this.idade = idade;
    this.altura = altura;
  }
  */


  //forma com código menor
  Pessoa(this.nome, this._idade, this.altura);

  //posso criar um construtor personalizado que não receba todos os dados
  //vai ser o construtor .nascer()
  Pessoa.nascer(this.nome, this.altura){
    this._idade = 0;

    print("$nome Nasceu!!");
    print("Altura do bebe $altura cm");
  }
  /* formas classicas de criar get e set
  int get idade {
    return _idade;
  }

  set idade(int idade) {
    if(idade > 0 && idade < 150){
      _idade = idade;
    }
  }
  */

  //simplificando set e get
  int get idade => _idade;

  set idade(int idade) => _idade = idade;
  //--------------------------------------//

  void dormir(){
    print("$nome está dormindo!");
  }

  void aniver(){
    _idade++;
    print("$nome agora tem $_idade anos");
  }
}

void main(){

  Pessoa pessoa1 = Pessoa("Bruno", 24, 1.80);
  pessoa1.aniver();
  pessoa1.dormir();

  print("altura do ${pessoa1.nome} é ${pessoa1.altura} metros");


  //Testando construtor nascer
  Pessoa.nascer("Enzo", 0.30);

  //Instanciando um objeto com o construtor nascer
  Pessoa nene = Pessoa.nascer("Natalia", 0.20);

  print(nene.nome);


}