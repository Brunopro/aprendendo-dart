class Valores {
  //com static a variavel deixa de ser do objeto e passa a ser da classe
 //não precisamos instanciar um objeto para acessar essa variavel
 static int vezesClicado;

}

class Pessoa {

}

void main() {
  Valores.vezesClicado = 2;

  //Depois de inicializada não é possivel mudar o valor de uma const
  const numero = 3;

  //para torvar a varivel instanciada em uma classe em uma const que n pode ser alterada
  //utilize final
  final Pessoa pessoa1 = Pessoa();

}