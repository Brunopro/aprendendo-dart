//Em Dart cada Classe só pode herdar de uma Classe, igual em java

class Animal {
  String nome;
  double peso;


  //Contrutor da Super Classe
  Animal(this.nome, this.peso);


  void comer() {
    print("$nome comeu!");
  }

  void fazerSom() {
    print("$nome fez algum som!");
  }
}

class Cachorro extends Animal{

  int fofura;

  //Criando construtor
  //Cachorro(String nome, double peso, this.fofura) : super(nome, peso);

  Cachorro(String nome, double peso, this.fofura) : super(nome, peso) {
    print("Objeto cachorro criado");
  }

  void brincar() {
    fofura += 10;
    print("Fofura do $nome aumentou para $fofura");
  }
}

class Gato extends Animal {

  //Construtor classe gato
  Gato(nome, peso) : super(nome, peso);

  bool estaAmigavel() {
    return true;
  }
}

void main() {
  //Instanciar objeto
  //Nome da classe, nome do objeto, = , Nome da classe
  Cachorro cachorro1 = Cachorro("Brutos", 10.0, -2);

  cachorro1.brincar();
  cachorro1.brincar();
  cachorro1.brincar();

  Gato gato1 = Gato("miau", 5.0);

  print("gato esta amigavel? ${gato1.estaAmigavel()}");


}
