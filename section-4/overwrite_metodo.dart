class Animal {
  String nome;
  double peso;

  Animal(this.nome, this.peso);

  //metodo comer sempre será igual para as classes que herdarem dela
  //se as classes filhas quiserem "personalizar" tais metodos das classes pais
  //precisam usar a sobrescrita de metodo(override)
  void comer() {
    print("$nome comeu!");
  }

  String fazerSom() {
    return "$nome fez algum som";
  }
}

class Cachorro extends Animal {
  int fofura;

  Cachorro(String nome, double peso, this.fofura) : super(nome, peso) {
    print("Objeto cachorro criado");
  }

  void brincar() {
    fofura += 10;
    print("Fofura do $nome aumentou para $fofura");
  }

  //sobrescrevendo metodo fazer som
  @override
  String fazerSom() {
    return "$nome fez auauau!!!";
  }

  //sobrescrevendo metodo da classe Object, toda classe, variavel herda de Object
  @override
  String toString() {
    return "nome : $nome | peso : $peso | fofura : $fofura";
  }
}

class Gato extends Animal {
  Gato(nome, peso) : super(nome, peso);

  bool estaAmigavel() {
    return true;
  }

  //sobrescrevendo metodo fazer som
  @override
  String fazerSom() {
    return "$nome fez miaaaaaau!!!";
  }
}

void main() {
  Cachorro cachorro1 = Cachorro("Brutos", 10.0, -2);
  
  //print onde alteramos o metodo toString
  print(cachorro1);

  Gato gato1 = Gato("miau", 5.0);

  //print com toString padrão
  print(gato1);

  print("metodo fazeSom(): ${gato1.fazerSom()}");
  print("metodo fazeSom(): ${cachorro1.fazerSom()}");
}
