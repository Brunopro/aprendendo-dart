


//antes era classe Animal {
abstract class Animal {
  String nome;
  double peso;

  Animal(this.nome, this.peso);

  void comer() {
    print("$nome comeu!");
  }

  //Em classes abstratas podemos criar metodos sem corpo
  //desta formas estamos obrigando as classes filhas a sobrescrever esse metodo
  String fazerSom();

  String toString() {
    return "nome : $nome | peso : $peso";
  }
}

class Cachorro extends Animal {
  int fofura;

  Cachorro(String nome, double peso, this.fofura) : super(nome, peso) {
    print("Objeto cachorro criado");
  }

  void brincar() {
    fofura += 10;
    print("Fofura do $nome aumentou para $fofura");
  }

  @override
  String fazerSom() {
    return "$nome fez auauau!!!";
  }

  @override
  String toString() {
    return "nome : $nome | peso : $peso | fofura : $fofura";
  }
}

class Gato extends Animal {
  Gato(nome, peso) : super(nome, peso);

  bool estaAmigavel() {
    return true;
  }

  @override
  String fazerSom() {
    return "$nome fez miaaaaaau!!!";
  }
}

void main() {

  //No nosso exemplo a classe animal é uma classe comum
  //outras classes podem herdar dela
  //Mas se eu quiser posso criar um objeto do tipo animal sem problemas
  
  /********** Antes de a classe Animal se tornar abstrata esse código estaava funcionando 
  Animal animal1 = Animal("Rex", 100.0);

  print(animal1.toString());
  */


  //Mas em muitos casos você vai criar uma classe que não fará sentido sozinha
  //Então o que vc fará vai ser criar colocar um abstract na criação da classe
  //Com isso você estara dizendo que não faz sentido criar um objeto através dessa classe abstrata

  //Classe animal agora só pode ser herdada

}
